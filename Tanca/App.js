import React from 'react';
import Navigation from './src/Navigation';
import {LoginScreen} from './src/screens';

function App() {
  return (
    <Navigation />
  );
}

export default App;
