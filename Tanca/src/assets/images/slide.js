export const slides = [
    {
        title: 'Chấm công qua GPS, Wifi, QR Code tích hợp sâu với AI',
        caption: '',
        url: require('./slide1.png') 
    },
    {
        title: 'Giao việc, quản lý công việc, quy trình và tiến độ',
        caption: '',
        url: require('./slide2.png')
    },
    {
        title: 'Ứng lương, nhận phiếu lương và tiền lương hàng tháng',
        caption: '',
        url: require('./slide3.png')
    },
    {
        title: 'Số hóa 100% giấy tờ trong doanh nghiệp',
        caption: '',
        url: require('./slide4.png')
    },
    {
        title: 'Quản lý các thông báo, bản tin nội bộ',
        caption: '',
        url: require('./slide5.png')
    },
    {
        title: 'Quản lý vị trí nhân viên trên bản đồ số',
        caption: '',
        url: require('./slide6.png')
    },
    {
        title: 'Đăng ký Ca làm, Xếp ca làm tự động',
        caption: '',
        url: require('./slide7.png')
    },
]