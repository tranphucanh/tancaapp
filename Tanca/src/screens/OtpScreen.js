import {useEffect, useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faXmark, faCaretDown} from '@fortawesome/free-solid-svg-icons';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  Alert,
} from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {accounts} from '../data/accounts';

function OtpScreen({navigation}) {
  const [time, setTime] = useState(60);
  const [otp, setOtp] = useState('');
  const [check, setCheck] = useState(false);

  useEffect(() => {
    accounts.map(account => {
      if (otp == account.otp) {
        setCheck(true);
      }
    });
  }, [otp]);

  useEffect(() => {
    if (time > 0) {
      setTimeout(() => {
        setTime(time - 1);
      }, 1000);
    }
  }, [time]);

  return (
    <ScrollView>
      <View style={styles.header}>
        <Image
          style={styles.gr12}
          source={require('../assets/images/Group12.png')}
        />

        <View style={styles.headerTop}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack('Intro');
            }}>
            <FontAwesomeIcon
              style={styles.headerIcon}
              icon={faXmark}
              size={30}
            />
          </TouchableOpacity>
          <View style={styles.headerTopRight}>
            <FontAwesomeIcon
              style={styles.headerIcon}
              icon={faCaretDown}
              size={22}
            />
            <Image
              style={styles.headerTopLanguage}
              source={require('../assets/images/VietNam.png')}
            />
          </View>
        </View>

        <View style={styles.headerLogo}>
          <Image source={require('../assets/images/LogoLogin.png')} />
        </View>

        <Image
          style={styles.gr11}
          source={require('../assets/images/Group11.png')}
        />
      </View>

      <View style={styles.body}>
        <View style={styles.bodyTitle}>
          <Text style={styles.bodyTextHeader}>Xác minh OTP</Text>
          <Text style={styles.bodyTextNormal}>
            Nhập mã OTP gửi đến +84397819657
          </Text>
        </View>

        <OTPInputView
          style={{width: '100%', height: 100}}
          pinCount={6}
          codeInputFieldStyle={styles.inputNotFocused}
          codeInputHighlightStyle={styles.inputFocused}
          onCodeFilled={code => {
            setOtp(code);
          }}
        />

        <TouchableOpacity
          style={styles.btnSignIn}
          onPress={() => {
            if (check) {
              navigation.navigate('Home');
            } else {
              Alert.alert('OTP is invalid!');
            }
          }}>
          <Text style={styles.btnSignInText}>Sign in</Text>
        </TouchableOpacity>

        <View style={styles.footer}>
          <Text style={styles.normal}>Gửi lại sau </Text>
          <Text style={styles.highlight}>{time}s</Text>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  //header style
  header: {
    height: 300,
    backgroundColor: '#f2fcfa',
    height: 330,
    overflow: 'hidden',
  },
  headerTop: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  headerIcon: {
    color: '#9DAACE',
  },
  headerTopRight: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 55,
    fontSize: 19,
  },
  headerTopLanguage: {
    width: 30,
    height: 30,
  },
  headerLogo: {
    position: 'relative',
    top: 70,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  gr11: {
    position: 'absolute',
    bottom: -50,
    right: 0,
    zIndex: -1,
  },
  gr12: {
    position: 'absolute',
    zIndex: -1,
  },

  //body style
  body: {
    margin: 20,
  },
  bodyTextHeader: {
    marginBottom: 10,
    fontSize: 23.43,
    fontWeight: '600',
    color: '#333',
  },
  bodyTextNormal: {
    marginBottom: 10,
    fontSize: 16.45,
    color: '#9DAACE',
  },
  inputNotFocused: {
    color: '#333',
    fontSize: 20,
    fontWeight: 'bold',
  },
  inputFocused: {
    borderColor: '#1ECC78',
    borderWidth: 2,
    color: '#333',
  },
  btnSignIn: {
    width: '100%',
    paddingVertical: 12,
    backgroundColor: '#1ECC78',
    borderWidth: 1,
    borderColor: '#1ECC78',
    borderRadius: 15,
    alignItems: 'center',
  },
  btnSignInText: {
    fontSize: 18.51,
    color: '#ffffff',
  },
  footer: {
    marginTop: 40,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  normal: {
    fontSize: 16.45,
  },
  highlight: {
    fontSize: 16.45,
    fontWeight: 'bold',
  },
});

export default OtpScreen;
