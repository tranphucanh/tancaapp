import {Image, StyleSheet, View} from 'react-native';

function LoadingScreen() {
  return (
    <View>
      <Image
        style={styles.img}
        source={require('../assets/images/Logo.png')}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  img: {
    position: 'relative',
    top: 300,
    left: 85,
    width: 233,
    height: 76,
  },
});

export default LoadingScreen;
