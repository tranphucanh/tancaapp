import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faXmark, faCaretDown} from '@fortawesome/free-solid-svg-icons';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  Alert,
} from 'react-native';
import PhoneInput from 'react-native-phone-number-input';
import {accounts} from '../data/accounts';
import {useEffect, useState} from 'react';

function LoginScreen({navigation}) {
  const [phoneNum, setPhoneNum] = useState('');
  const [check, setCheck] = useState(false);

  useEffect(() => {
    accounts.map(account => {
      if (phoneNum == account.phoneNum) {
        setCheck(true);
      }
    });
  }, [phoneNum]);

  return (
    <ScrollView>
      <View style={styles.header}>
        <Image
          style={styles.gr12}
          source={require('../assets/images/Group12.png')}
        />

        <View style={styles.headerTop}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack('Intro');
            }}>
            <FontAwesomeIcon
              style={styles.headerIcon}
              icon={faXmark}
              size={30}
            />
          </TouchableOpacity>
          <View style={styles.headerTopRight}>
            <FontAwesomeIcon
              style={styles.headerIcon}
              icon={faCaretDown}
              size={22}
            />
            <Image
              style={styles.headerTopLanguage}
              source={require('../assets/images/VietNam.png')}
            />
          </View>
        </View>

        <View style={styles.headerLogo}>
          <Image source={require('../assets/images/LogoLogin.png')} />
        </View>

        <Image
          style={styles.gr11}
          source={require('../assets/images/Group11.png')}
        />
      </View>

      <View style={styles.body}>
        <View style={styles.bodyTitle}>
          <Text style={styles.bodyTextHeader}>Login</Text>
          <Text style={styles.bodyTextNormal}>Hello, so glad you're back</Text>
        </View>

        <PhoneInput
          containerStyle={styles.inputPhoneNumber}
          textInputStyle={styles.inputPhoneNumberText}
          defaultCode="VN"
          layout="first"
          withShadow
          value={phoneNum}
          onChangeFormattedText={text => setPhoneNum(text)}
        />

        <TouchableOpacity
          style={styles.btnSignIn}
          onPress={() => {
            if (check) {
              navigation.navigate('OTP');
            } else {
              Alert.alert('Please enter a valid phone number!');
            }
          }}>
          <Text style={styles.btnSignInText}>Sign in</Text>
        </TouchableOpacity>

        <View style={styles.footer}>
          <Text style={styles.normal}>Sign in with </Text>
          <Text style={styles.highlight}>Azure AD</Text>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  //header style
  header: {
    height: 300,
    backgroundColor: '#f2fcfa',
    height: 330,
    overflow: 'hidden',
  },
  headerTop: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  headerIcon: {
    color: '#9DAACE',
  },
  headerTopRight: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 55,
    fontSize: 19,
  },
  headerTopLanguage: {
    width: 30,
    height: 30,
  },
  headerLogo: {
    position: 'relative',
    top: 70,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  gr11: {
    position: 'absolute',
    bottom: -50,
    right: 0,
    zIndex: -1,
  },
  gr12: {
    position: 'absolute',
    zIndex: -1,
  },

  //body style
  body: {
    margin: 20,
  },
  bodyTextHeader: {
    marginBottom: 10,
    fontSize: 23.43,
    fontWeight: '600',
    color: '#333',
  },
  bodyTextNormal: {
    marginBottom: 20,
    fontSize: 16.45,
    color: '#9DAACE',
  },
  inputPhoneNumber: {
    width: '100%',
    marginBottom: 20,
    backgroundColor: '#f2f7ff',
    borderRadius: 8,
  },
  inputPhoneNumberText: {
    padding: 0,
    color: '#333',
    fontSize: 18,
    fontWeight: '500',
  },
  btnSignIn: {
    width: '100%',
    paddingVertical: 12,
    backgroundColor: '#1ECC78',
    borderWidth: 1,
    borderColor: '#1ECC78',
    borderRadius: 15,
    alignItems: 'center',
  },
  btnSignInText: {
    fontSize: 18.51,
    color: '#ffffff',
  },
  footer: {
    marginTop: 40,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  normal: {
    fontSize: 16.45,
  },
  highlight: {
    fontSize: 16.45,
    fontWeight: 'bold',
  },
});

export default LoginScreen;
