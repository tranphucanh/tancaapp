import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faXmark, faCaretDown} from '@fortawesome/free-solid-svg-icons';
import {
  Image,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

function HomeScreen({navigation}) {
  return (
    <ScrollView>
      <View style={styles.header}>
        <Image
          style={styles.gr12}
          source={require('../assets/images/Group12.png')}
        />

        <View style={styles.headerTop}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack('Intro');
            }}>
            <FontAwesomeIcon
              style={styles.headerIcon}
              icon={faXmark}
              size={30}
            />
          </TouchableOpacity>
          <View style={styles.headerTopRight}>
            <FontAwesomeIcon
              style={styles.headerIcon}
              icon={faCaretDown}
              size={22}
            />
            <Image
              style={styles.headerTopLanguage}
              source={require('../assets/images/VietNam.png')}
            />
          </View>
        </View>

        <View style={styles.headerLogo}>
          <Image source={require('../assets/images/LogoLogin.png')} />
        </View>

        <Image
          style={styles.gr11}
          source={require('../assets/images/Group11.png')}
        />
      </View>

      <View style={styles.body}>
        <View style={styles.bodyTitle}>
          <Text style={styles.bodyTextHeader}>Successful!</Text>
          <Text style={styles.bodyTextNormal}>Welcome to Tanca!</Text>
        </View>

        <View style={styles.footer}>
          <Text style={styles.normal}>Sign in with </Text>
          <Text style={styles.highlight}>Azure AD</Text>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  //header style
  header: {
    height: 300,
    backgroundColor: '#f2fcfa',
    height: 330,
    overflow: 'hidden',
  },
  headerTop: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  headerIcon: {
    color: '#9DAACE',
  },
  headerTopRight: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 55,
    fontSize: 19,
  },
  headerTopLanguage: {
    width: 30,
    height: 30,
  },
  headerLogo: {
    position: 'relative',
    top: 70,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  gr11: {
    position: 'absolute',
    bottom: -50,
    right: 0,
    zIndex: -1,
  },
  gr12: {
    position: 'absolute',
    zIndex: -1,
  },

  //body style
  body: {
    margin: 20,
  },
  bodyTitle: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  bodyTextHeader: {
    marginBottom: 10,
    fontSize: 23.43,
    fontWeight: '600',
    color: '#333',
  },
  bodyTextNormal: {
    marginBottom: 20,
    fontSize: 16.45,
    color: '#9DAACE',
  },
  footer: {
    marginTop: 150,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  normal: {
    fontSize: 16.45,
  },
  highlight: {
    fontSize: 16.45,
    fontWeight: 'bold',
  },
});

export default HomeScreen;
