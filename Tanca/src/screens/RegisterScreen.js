import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faXmark,
  faCaretDown,
  faUserGroup,
} from '@fortawesome/free-solid-svg-icons';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  TextInput,
  Alert,
} from 'react-native';
import PhoneInput from 'react-native-phone-number-input';
import {useState} from 'react';

function RegisterScreen({navigation}) {
  const [name, setName] = useState('');
  const [phoneNum, setPhoneNum] = useState('');

  return (
    <ScrollView>
      <View style={styles.header}>
        <Image
          style={styles.gr12}
          source={require('../assets/images/Group12.png')}
        />

        <View style={styles.headerTop}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack('Intro');
            }}>
            <FontAwesomeIcon
              style={styles.headerIcon}
              icon={faXmark}
              size={30}
            />
          </TouchableOpacity>
          <View style={styles.headerTopRight}>
            <FontAwesomeIcon
              style={styles.headerIcon}
              icon={faCaretDown}
              size={22}
            />
            <Image
              style={styles.headerTopLanguage}
              source={require('../assets/images/VietNam.png')}
            />
          </View>
        </View>

        <View style={styles.headerLogo}>
          <Image source={require('../assets/images/LogoLogin.png')} />
        </View>

        <Image
          style={styles.gr11}
          source={require('../assets/images/Group11.png')}
        />
      </View>

      <View style={styles.body}>
        <View style={styles.bodyTitle}>
          <Text style={styles.bodyTextHeader}>Register</Text>
          <Text style={styles.bodyTextNormal}>Tell us about you</Text>
        </View>

        <View style={styles.inputName}>
          <FontAwesomeIcon style={styles.inputNameIcon} icon={faUserGroup} />
          <TextInput
            value={name}
            style={styles.inputNameChild}
            placeholder="Your full name"
            onChangeText={text => {
              setName(text);
            }}
          />
        </View>

        <PhoneInput
          value={phoneNum}
          containerStyle={styles.inputPhoneNumber}
          textInputStyle={styles.inputPhoneNumberText}
          defaultCode="VN"
          layout="first"
          withShadow
          onChangeText={text => {
            setPhoneNum(text);
          }}
        />

        <TouchableOpacity
          style={styles.btnSignIn}
          onPress={() => {
            if (name != '' && phoneNum != '') {
              navigation.navigate('Home');
            } else {
              Alert.alert('Please enter a name and phone number');
            }
          }}>
          <Text style={styles.btnSignInText}>Register</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  //header style
  header: {
    height: 300,
    backgroundColor: '#f2fcfa',
    height: 330,
    overflow: 'hidden',
  },
  headerTop: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  headerIcon: {
    color: '#9DAACE',
  },
  headerTopRight: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 55,
    fontSize: 19,
  },
  headerTopLanguage: {
    width: 30,
    height: 30,
  },
  headerLogo: {
    position: 'relative',
    top: 70,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  gr11: {
    position: 'absolute',
    bottom: -50,
    right: 0,
    zIndex: -1,
  },
  gr12: {
    position: 'absolute',
    zIndex: -1,
  },

  //body style
  body: {
    margin: 20,
  },
  bodyTextHeader: {
    marginBottom: 10,
    fontSize: 23.43,
    fontWeight: '600',
    color: '#333',
  },
  bodyTextNormal: {
    marginBottom: 20,
    fontSize: 16.45,
    color: '#9DAACE',
  },
  inputName: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    backgroundColor: '#f2f7ff',
  },
  inputNameIcon: {
    padding: 10,
    marginHorizontal: 10,
    color: '#9DAACE',
  },
  inputNameChild: {
    flex: 1,
    fontSize: 18,
    fontWeight: '500',
  },
  inputPhoneNumber: {
    width: '100%',
    height: 65,
    marginBottom: 20,
    backgroundColor: '#f2f7ff',
    borderRadius: 8,
  },
  inputPhoneNumberText: {
    padding: 0,
    color: '#333',
    fontSize: 18,
    fontWeight: '500',
  },
  btnSignIn: {
    width: '100%',
    paddingVertical: 12,
    backgroundColor: '#1ECC78',
    borderWidth: 1,
    borderColor: '#1ECC78',
    borderRadius: 15,
    alignItems: 'center',
  },
  btnSignInText: {
    fontSize: 18.51,
    color: '#ffffff',
  },
  footer: {
    marginTop: 40,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  normal: {
    fontSize: 16.45,
  },
  highlight: {
    fontSize: 16.45,
    fontWeight: 'bold',
  },
});

export default RegisterScreen;
