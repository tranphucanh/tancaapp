import IntroScreen from "./IntroScreen";
import LoginScreen from "./LoginScreen";
import RegisterScreen from "./RegisterScreen";
import OtpScreen from "./OtpScreen";
import HomeScreen from "./HomeScreen";
import LoadingScreen from './LoadingScreen';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator()

const RootComponent = () => {
    return (
        <NavigationContainer>
          <Stack.Navigator initialRouteName='Intro' screenOptions={{headerShown: false}}>
            <Stack.Screen name="Loading" component={LoadingScreen}/>
            <Stack.Screen name="Intro" component={IntroScreen}/>
            <Stack.Screen name="Login" component={LoginScreen}/>
            <Stack.Screen name="OTP" component={OtpScreen}/>
            <Stack.Screen name="Register" component={RegisterScreen}/>
            <Stack.Screen name="Home" component={HomeScreen}/>
          </Stack.Navigator>
        </NavigationContainer>
    );
}

export default RootComponent;