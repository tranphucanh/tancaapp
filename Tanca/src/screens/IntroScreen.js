import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ImagesShow from '../components/ImagesShow';

function IntroScreen({navigation}) {
  return (
    <View>
      <ImagesShow />
      <View style={styles.btn}>
        <TouchableOpacity
          style={styles.btnLog}
          onPress={() => {
            navigation.navigate('Login');
          }}>
          <Text style={styles.btnLogText}>Log in</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btnJoin}
          onPress={() => {
            navigation.navigate('Register');
          }}>
          <Text style={styles.btnJoinText}>Join Now</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.footer}>
        <Text style={styles.normal}>Sign in with </Text>
        <Text style={styles.highlight}>Azure AD</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  img: {
    position: 'relative',
    top: 150,
    left: 35,
    marginTop: 150,
  },
  title: {
    position: 'absolute',
    top: 390,
    left: 30,
    maxWidth: 336,
    paddingHorizontal: 15,
    paddingVertical: 10,
    color: '#333',
    fontSize: 23.43,
    fontWeight: '600',
    textAlign: 'center',
  },
  btn: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    position: 'absolute',
    top: 550,
    width: '100%',
  },
  btnLog: {
    width: 170,
    paddingHorizontal: 45,
    paddingVertical: 12,
    borderWidth: 1,
    borderColor: '#1ECC78',
    borderRadius: 10,
    alignItems: 'center',
  },
  btnLogText: {
    fontSize: 18.51,
    color: '#1ECC78',
  },
  btnJoin: {
    width: 170,
    paddingHorizontal: 45,
    paddingVertical: 12,
    backgroundColor: '#1ECC78',
    borderWidth: 1,
    borderColor: '#1ECC78',
    borderRadius: 10,
    alignItems: 'center',
  },
  btnJoinText: {
    fontSize: 18.51,
    color: '#ffffff',
  },
  footer: {
    top: 650,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  normal: {
    fontSize: 16.45,
  },
  highlight: {
    fontSize: 16.45,
    fontWeight: 'bold',
  },
});

export default IntroScreen;
