export const accounts = [
    {
        id: 1,
        phoneNum: +840397819657,
        otp: 123456,
    },
    {
        id: 2,
        phoneNum: +840397819567,
        otp: 234567,
    },
    {
        id: 3,
        phoneNum: +840397819123,
        otp: 345678,
    },
    {
        id: 4,
        phoneNum: +840397819321,
        otp: 456789,
    },
    {
        id: 5,
        phoneNum: +840397819765,
        otp: 567890,
    },
]