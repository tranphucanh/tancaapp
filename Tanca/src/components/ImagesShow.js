import * as React from 'react';
import {StyleSheet} from 'react-native';
import {slides} from '../assets/images/slide';
import Slideshow from 'react-native-image-slider-show';

export default class ImagesShow extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      position: 1,
      interval: null,
      dataSource: slides,
    };
  }

  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position:
            this.state.position === this.state.dataSource.length
              ? 0
              : this.state.position + 1,
        });
      }, 2000),
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.interval);
  }

  render() {
    return (
      <Slideshow
        dataSource={this.state.dataSource}
        position={this.state.position}
        onPositionChanged={position => this.setState({position})}
        containerStyle={styles.containerStyle}
        titleStyle={styles.titleStyle}
        height={320}
      />
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    marginTop: 130,
    backgroundColor: '#fff',
    overflow: 'visible',
    zIndex: 1,
  },
  titleStyle: {
    position: 'absolute',
    bottom: -30,
    width: '108%',
    height: 80,
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: 'rgba(237, 238, 239, 0.5)',
    color: '#333',
    fontSize: 23.43,
    fontWeight: '600',
    textAlign: 'center',
    zIndex: 999,
  },
});
